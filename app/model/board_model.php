<?php

class BoardModel {
  private $db;
  
  function __construct() {
    $this->db = new Connection();
  }

  public function displayBoard() {
    $query = mysqli_query($this->db->getConnection() ,"SELECT * FROM `bulletin` ORDER BY `id` DESC");

    while($data = mysqli_fetch_array($query)){
      $result[] = $data;
    }
    return $result;
  }

  public function save($title, $body) {
    $query = " INSERT INTO `bulletin`(`bulletin_title`, `bulletin_body`, `create_at`) " 
            . " VALUES ('{$title}', '{$body}', now()) ";

    $save = mysqli_query($this->db->getConnection(), $query);

    return $save;
  }
}