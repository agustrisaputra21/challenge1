<?php

class Connection {
  private $db;
  private $connection;

  function __construct() {
    $this->db = new Database();
  }

  function getConnection() {
    $connect = new mysqli($this->db->HOST, $this->db->USERNAME, $this->db->PASSWORD, $this->db->DATABASE);

    if ( $connect->connect_errno) {
      echo "connection failed";
      exit();
    } 

    return $this->connection = $connect;
  }
}