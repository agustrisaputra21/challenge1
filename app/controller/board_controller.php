<?php

class BoardController {
  private $model;

  function __construct() {
    $this->model = new BoardModel();
  }

  function display(){
    return $this->model->displayBoard();
  }

  function save($title, $body) {
    return $this->model->save($title, $body);
  }
}