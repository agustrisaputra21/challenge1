-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Des 2019 pada 04.09
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bulletin_board`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bulletin`
--

CREATE TABLE `bulletin` (
  `id` int(11) NOT NULL,
  `bulletin_title` varchar(50) NOT NULL,
  `bulletin_body` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `bulletin`
--

INSERT INTO `bulletin` (`id`, `bulletin_title`, `bulletin_body`, `create_at`) VALUES
(3, 'test 123455', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. A eum dolorum voluptates tenetur assumenda hic minus, dolor cum magni, nisi similique illo totam inventore', '2019-12-03 09:17:50'),
(4, 'test 123455', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. A eum dolorum voluptates tenetur assumenda hic minus, dolor cum magni, nisi similique illo totam inventore', '2019-12-03 09:32:44'),
(5, 'title test 1', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. A eum dolorum voluptates tenetur assumenda hic minus, dolor cum magni, nisi similique illo totam inventore', '2019-12-03 09:33:17'),
(6, 'title test 2', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. A eum dolorum voluptates tenetur assumenda hic minus, dolor cum magni, nisi similique illo totam inventore', '2019-12-03 10:11:57'),
(7, 'title test 3', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur officia facere dolores fugit quos voluptatem ad placeat nobis sit esse. Est illo ducimus error!', '2019-12-03 10:31:29'),
(8, 'title test 3', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur officia facere dolores fugit quos voluptatem ad placeat nobis sit esse. Est illo ducimus error!', '2019-12-03 10:32:13'),
(9, 'title test 4', 'loremmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm', '2019-12-03 10:34:21'),
(10, 'test title 123', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.dolor adipisci possimus, repellendus culpa repellat ad harum fugiat praesentium non voluptate qui numquam odio?', '2019-12-03 11:08:46');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bulletin`
--
ALTER TABLE `bulletin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `bulletin`
--
ALTER TABLE `bulletin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
