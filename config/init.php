<?php

  require_once(__DIR__."/database.php");
  require(__DIR__."/../app/connection.php");

  require(__DIR__."/../app/controller/board_controller.php");
  
  require(__DIR__."/../app/model/board_model.php");

  require(__DIR__."/../view/index.php");
