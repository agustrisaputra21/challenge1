<?php
  
  $controll = new BoardController();

  $title = "";
  $body = "";
  $titleError = "";
  $bodyError = "";
  $error = false;

  if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {
    $title = $_POST["title"];
    $body = $_POST["body"];

    if ( (strlen ( $title ) < 10 || 
          strlen ( $title ) > 32 ) || 
          empty($title)) {
      $titleError = "Your title must be 10 to 32 character long.";
      $error = true;
    }
    
    if ( (strlen ( $body ) < 10 || 
          strlen ( $body ) > 200 ) || 
          empty($body)) {
      $bodyError = "Your body must be 10 to 200 character long.";
      $error = true;
    }

    if ( !$error ) {
      $save = $controll->save($title, $body);

      if ( $save ) {

        $title = "";
        $body = "";
        
        header('Location: index.php');
      }
    }
  }

  $dataList = $controll->display();
  
  
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bulletin Board</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<style>
  .error {color: #FF0000;}
</style>

<body>
  <div class="container">
    <div class="row justify-content-md-center">
      <form method="POST">
        <div class="form-group">
          <label for="bulletinTitle">Title</label>
          <input type="text" class="form-control" id="bulletinTitleInput" name="title" value="<?php echo $title;?>">
          <span class="error">
            <?php echo $titleError;?>
          </span>
        </div>
        <div class="form-group">
          <label for="bulletinBody">Body</label>
          <textarea class="form-control" id="bulletinBodyInput" name="body" rows="3" ><?php echo $body;?></textarea>
          <span class="error">
            <?php echo $bodyError;?>
          </span>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
    <br>
    <div class="row">
      <?php if (count($dataList) > 0) :?>
        <div class="list-group">
          <?php foreach($dataList as $row) : ?>
            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
              <h5 class="mb-1"><?php echo $row["bulletin_title"]; ?></h5>
              <p class="mb-1"><?php echo $row["bulletin_body"]; ?></p>
              <small class="float-right"><?php echo date('d-m-Y H:i:s', strtotime($row["create_at"])); ?></small>
            </a>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>  
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>